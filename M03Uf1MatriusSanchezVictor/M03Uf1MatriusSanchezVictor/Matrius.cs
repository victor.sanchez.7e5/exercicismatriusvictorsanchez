﻿public class Matrius
{
    static void Main()
    {
        SimpleBattleshipResult(); 


    }

    /// <summary>
    /// Donada la següent configuració del joc Enfonsar la flota, indica si a la posició (x, y) hi ha aigua o un vaixell (tocat)
    /// </summary>
    static void SimpleBattleshipResult()
    {                       
        string[,] tauler = new string[,] { { "x", "x", "0", "0", "0", "0", "x" }, 
                                           { "0", "0", "x", "0", "0", "0", "x" }, 
                                           { "0", "0", "0", "0", "0", "0", "x" },
                                           { "0", "x", "x", "x", "0", "0", "x" },
                                           { "0", "0", "0", "0", "x", "0", "0" },
                                           { "0", "0", "0", "0", "x", "0", "0" },
                                           { "x", "0", "0", "0", "0", "0", "0" }, 
                                          };

        /*
        Console.WriteLine                 ("0  1  2  3  4  5  6 ");
        Console.WriteLine                 ("x  x  0  0  0  0  x     - 0");
        Console.WriteLine                 ("0  0  x  0  0  0  x     - 1" );
        Console.WriteLine                 ("0  0  0  0  0  0  x     - 2");
        Console.WriteLine                 ("0  x  x  x  0  0  x     - 3");
        Console.WriteLine                 ("0  0  0  0  x  0  0     - 4");
        Console.WriteLine                 ("0  0  0  0  x  0  0     - 5");
        Console.WriteLine                 ("x  0  0  0  0  0  0     - 6");
        */
        Console.WriteLine("Posició en el tauler en X");
        int y = Convert.ToInt32(Console.ReadLine());    
        Console.WriteLine("Posició en el tauler en Y");
        int x = Convert.ToInt32(Console.ReadLine());

        if (tauler[x, y] == "x") Console.WriteLine("TOCAT !");
        else Console.WriteLine ("AIGUA");          
    }


}    
